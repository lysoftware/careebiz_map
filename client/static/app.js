
var map, directionsService, directionDisplay;
function initMap() { // google maps api

    directionsService = new google.maps.DirectionsService();

    directionsDisplay = new google.maps.DirectionsRenderer({draggable: true});

    var MyOptions = {
        zoom: 12,
        center: {lat: 32.063015, lng: 34.785940},
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false
    };

    map = new google.maps.Map(document.getElementById('map-zone'), MyOptions);

    directionsDisplay.setMap(map);

    // Add a marker to the map for the end-point of the directions.
    var marker = new google.maps.Marker({
        position: {lat: 32.063015, lng: 34.785940}, 
        map: map, 
        title:"Careebiz"
    }); 

}

function changeTravelMode (itemId) {
    // first set everyone as non-chosen
    for (let i = 0; i < $('#ways-zone').children().length; i++) {
        let childIdId = $('#ways-zone').children()[i].id;
        $('#' + childIdId).removeClass('selected');
    }
    //set the selected item with green
    $('#' + itemId).addClass('selected');
}

function calcRoute () {
    let travelMode;
    let selectedItemId = $('.selected')[0].id; 
    switch (selectedItemId) {
        case 'train_div':
            travelMode = 'TRANSIT';
            break;
        case 'walk_div':
            travelMode = 'WALKING';
            break;
        case 'bicycle_div':
            travelMode = 'BICYCLING';
            break;
        default:
            travelMode = 'DRIVING';
    }

    let startPoint = $('#search-input').val();
    if (!startPoint) startPoint = 'HaBanim Street 115, Ness Ziona'; // come visit sometime:-)

    let endPoint = '32.063015,34.785940';

    let Request = {
        origin: startPoint,
        destination: endPoint,
        waypoints: [],
        unitSystem: google.maps.UnitSystem.IMPERIAL,
        travelMode: google.maps.DirectionsTravelMode[travelMode]
    };

    directionsService.route(Request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            $('#service-message').text('');
            directionsDisplay.setDirections(response);
        } else {
            console.error(status, getDetailedErrorMsg(status));
            $('#service-message').text(getDetailedErrorMsg(status));
        }
    });
}

function getDetailedErrorMsg (serviceErr) {
    let res = '';
    switch (serviceErr) {
        case 'ZERO_RESULTS':
            res = 'No route could be found between the origin and destination';
            break;
        case 'UNKNOWN_ERROR':
            res = 'A directions request could not be processed due to a server error. The request may succeed if you try again';
            break;
        case 'REQUEST_DENIED':
            res = 'This webpage is not allowed to use the directions service - make sure a valid API key is used';
            break;
        case 'OVER_QUERY_LIMIT':
            res = 'The webpage has gone over the requests limit in too short a period of time';
            break;
        case 'NOT_FOUND':
            res = 'At least one of the origin, destination, or waypoints could not be geocoded';
            break;
        case 'INVALID_REQUEST':
            res = 'The DirectionsRequest provided was invalid';
            break;
        default:
            res = 'There was an unknown error in your request. Requeststatus: ' + serviceErr;
    }

    return res;
}
